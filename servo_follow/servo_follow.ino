
#include <Servo.h>

Servo myservo;

//Definitions
unsigned long prevMillis_PIRread = 0; 
unsigned long prevMillis_baseInc = 0; 
unsigned long prevMillis_servo = 0; 
unsigned long prevMillis_debug = 0; 

//int PWM_servoOut = 512; //2.42x 
int servoSweep = 90; //0-180 degrees
bool servoDirection = 0; // 0=up,1=down

byte PIR_array[15]; // One element for each PIR sensor's reading


void setup() 
{
  delay(1000); // Safety delay
  Serial.begin(9600);
  Serial.println("### SETUP - Servo Test ###");
  pinMode(12, OUTPUT);

  myservo.attach(5);

  Serial.println(sizeof(PIR_array));
  
}

void loop() 
{
  unsigned long currentMillis = millis();
  

  if (currentMillis - prevMillis_debug >= 10UL)
  {
    prevMillis_debug = currentMillis;


    //Serial.print("Servo=");
    //Serial.println(analogRead(5));
  }

  if (currentMillis - prevMillis_servo >= 100UL)
  {
    prevMillis_servo = currentMillis;
    
    //Servo sweep
    //530-93=437, 2.42x
    if (servoSweep <= 0)
    {
      servoDirection = 0;
    }
    else if (servoSweep >= 180 )
    {
      servoDirection = 1;
    }
    
    if (servoDirection == 0)
    {
      servoSweep++;
    }
    else if (servoDirection == 1 )
    {
      servoSweep--;
    }
    
    myservo.write(servoSweep); 

  
    if (Serial.available() > 2)
    {
      byte serialBytes[3] = {0, 0, 0};
      byte lineEnding = 0x0A; //10 in decimal, ASCII newline character
      int bytesRead = Serial.readBytesUntil(lineEnding, (char*)serialBytes, 3);
      servoSweep = ((serialBytes[0]-48)*100)+((serialBytes[1]-48)*10)+(serialBytes[2]-48);
      Serial.print("Serial Received:");
      Serial.println(servoSweep);
    }

    Serial.print("Sweep=");
    Serial.print(servoSweep);
    Serial.print(",Dir=");
    Serial.print(servoDirection);
    Serial.print(",Feedback=");
    Serial.print(analogRead(5));
    Serial.print(",Mapped=");
    Serial.println(map(analogRead(5), 93, 530, 0, 180));

    
  }

  
  
}



void PIRcrunch()
{ 
  
  byte PIR_currentmax = 0;
  for (int i=0; i <= sizeof(PIR_array); i++)
  {
    if (PIR_array[i] > PIR_currentmax) 
    {
      PIR_currentMaxValue_byte = PIR_array[i];
      PIR_currentMaxSensorNum_byte = [i];
    }
  } 
  PIR_currentMaxSensorNum_byte


}

